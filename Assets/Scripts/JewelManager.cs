﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class JewelManager : MonoBehaviour
{
    [Header("Initial Setup")]
    public Vector2 startingPos;
    public bool setupJewel;
    public float initialFallSpeed;
    public float currentDifference;

    [Header("Moving jewels")]
    public bool moveJewel;
    public Vector2 futurePos;
    public int currentLine;
    public int currentColumn;
    public BoardManager boardManager;
    public int maxLine;
    public int maxColumn;
    public JewelManager otherJewelManager;
    public RaycastHit jewelHitted;

    [Header("Move Raycast")]
    public bool repositionObject;
    public Vector2 firstPressPos;
    public Vector2 secondPressPos;
    public Vector2 currentSwipe;
    public Vector3 mouseWorldPosition;
    public Camera cam;
    public float camRayLenght;
    int jewelMask;
    public bool checkSecondPos;
    public float moveRayLenght;
    public RaycastHit buttonHit;

    [Header("Check Next Raycast")]
    public bool callOtherRaycast;
    public float checkRayLenght;
    public RaycastHit upHit;
    public RaycastHit downHit;
    public RaycastHit leftHit;
    public RaycastHit rightHit;
    public GameObject objAbove;
    public GameObject objBelow;
    public GameObject objLeft;
    public GameObject objRight;
    public bool upIsCorrect;
    public bool downIsCorrect;
    public bool leftIsCorrect;
    public bool rightIsCorrect;
    public JewelManager whoCalledMe;
    public bool foundMatchHor;
    public bool foundMatchVer;
    public bool justTellOnce;

    [Header("Return to previous position")]
    public bool repositionJewel;
    public bool repositionJewelConfirmation;
    public bool iWasTheOneWhoCalled;
    public Vector2 previousPos;
    public int wrongLine;
    public int wrongColumn;
    public JewelManager jewelMovedTogether;

    [Header("Moving all Jewels down")]
    public bool moveJewelDown;
    public Vector2 moveDownPos;

    [Header("Asking To Create New Jewel")]
    public bool callAnotherJewel;

    [Header("Instantiating Explosion")]
    public GameObject explosion;

    [Header("Moving by Selection")]
    public bool selected;
    public GameObject selectionObj;
    bool upSelected;
    bool downSelected;
    bool leftSelected;
    bool rightSelected;
    public bool tempCheckAbove;
    public bool tempCheckBelow;
    public bool tempCheckLeft;
    public bool tempCheckRight;

    [Header("Debug")]
    public string thisTag;
    public bool iAlreadyChecked;
    public bool tryAgain;
    //public TextMeshProUGUI firstMousePos;

    void Start()
    {
        cam = Camera.main;
        jewelMask = LayerMask.GetMask("Jewel");
        boardManager = FindObjectOfType<BoardManager>();
        maxLine = boardManager.height -1;
        maxColumn = boardManager.width -1;
        setupJewel = true;
    }

    void Update()
    {
        thisTag = gameObject.tag;

        Swipe();

        if (setupJewel)
        {
            InitialSetup();
        }

        if (moveJewel)
        {
            MoveJewel();
        }        
        
        if (moveJewelDown)
        {
            MoveJewelDown();
        }

        if (tryAgain)
        {
            MakeObjectsGoDown();
        }

        if (callOtherRaycast)
        {
            CheckJewelsAround();
        }

        if (repositionJewel && repositionJewelConfirmation)
        {
            MoveJewelBack();
        }
    }

    public void InitialSetup()
    {
        currentDifference = transform.position.y - startingPos.y;
        transform.position = Vector2.Lerp(transform.position, startingPos, initialFallSpeed);
        if (currentDifference < 0.001f)
        {
            transform.position = new Vector3(Mathf.Abs(transform.position.x), Mathf.Abs(transform.position.y), transform.position.z);

            currentColumn = Mathf.RoundToInt(transform.position.x);
            currentLine = Mathf.RoundToInt(transform.position.y);
            setupJewel = false;

            if (callAnotherJewel)
            {
                iAlreadyChecked = false;
                callOtherRaycast = true;
                //MakeObjectsGoDown();
                GetAllObjectsAbove();
            }
        }
    }

    public void MoveJewel()
    {
        selected = false;
        selectionObj.SetActive(false);
        float currentXDifference = transform.position.x - futurePos.x;
        float currentYDifference = transform.position.y - futurePos.y;
        transform.position = Vector2.Lerp(transform.position, futurePos, initialFallSpeed);
        if (currentXDifference < 0.001f && currentXDifference > -0.001f &&
            currentYDifference < 0.001f && currentYDifference > -0.001f)
        {
            transform.position = new Vector3(Mathf.Abs(transform.position.x), Mathf.Abs(transform.position.y), transform.position.z);

            currentColumn = Mathf.RoundToInt(transform.position.x);
            currentLine = Mathf.RoundToInt(transform.position.y);

            repositionJewel = false;
            repositionJewelConfirmation = false;

            RaycastHit tempUpHit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out tempUpHit, 1f, jewelMask))
            {
                tempUpHit.transform.GetComponent<JewelManager>().MakeObjectsGoDown();
            }

            iAlreadyChecked = false;
            callOtherRaycast = true;
            moveJewel = false;
        }
    }

    public void MoveJewelDown()
    {
        float currentXDifference = transform.position.x - moveDownPos.x;
        float currentYDifference = transform.position.y - moveDownPos.y;
        transform.position = Vector2.Lerp(transform.position, moveDownPos, initialFallSpeed);
        if (currentXDifference < 0.001f && currentXDifference > -0.001f &&
            currentYDifference < 0.001f && currentYDifference > -0.001f)
        {
            transform.position = new Vector3(Mathf.Abs(transform.position.x), Mathf.Abs(transform.position.y), transform.position.z);

            currentColumn = Mathf.RoundToInt(transform.position.x);
            currentLine = Mathf.RoundToInt(transform.position.y);

            repositionJewel = false;
            repositionJewelConfirmation = false;

            iAlreadyChecked = false;
            callOtherRaycast = true;

            RaycastHit tempUpHit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out tempUpHit, 1f, jewelMask))
            {
                tempUpHit.transform.GetComponent<JewelManager>().MakeObjectsGoDown();
            }

            MakeObjectsGoDown();
            GetAllObjectsAbove();

            moveJewelDown = false;
        }
    }

    public void MoveJewelBack()
    {
        float currentXDifference = transform.position.x - previousPos.x;
        float currentYDifference = transform.position.y - previousPos.y;
        transform.position = Vector2.Lerp(transform.position, previousPos, initialFallSpeed);
        if (currentXDifference < 0.001f && currentXDifference > -0.001f &&
            currentYDifference < 0.001f && currentYDifference > -0.001f)
        {
            transform.position = new Vector3(Mathf.Abs(transform.position.x), Mathf.Abs(transform.position.y), transform.position.z);

            currentColumn = Mathf.RoundToInt(transform.position.x);
            currentLine = Mathf.RoundToInt(transform.position.y);

            repositionJewel = false;
            repositionJewelConfirmation = false;
        }
    }

    public void Swipe()
    {
        if (Input.GetMouseButtonDown(0))
        {
            iAlreadyChecked = false;

            objLeft = null;
            objAbove = null;
            objBelow = null;
            objRight = null;

            whoCalledMe = null;

            justTellOnce = true;

            MovementRaycast();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (checkSecondPos)
            {
                mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);

                secondPressPos = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);

                currentSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

                currentSwipe.Normalize();

                previousPos.x = Mathf.Round(transform.position.x);
                previousPos.y = Mathf.Round(transform.position.y);

                //swipe up
                if (currentSwipe.y > 0.75f && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f && currentLine < maxLine)
                {
                    UpSwipe();
                }
                else
                {
                    selected = true;
                    selectionObj.SetActive(true);
                    CheckSelected();
                }

                //swipe down
                if (currentSwipe.y < -0.75f && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f && currentLine > 0)
                {
                    DownSwipe();
                }
                else
                {
                    selected = true;
                    selectionObj.SetActive(true);
                    CheckSelected();
                }

                //swipe left
                if (currentSwipe.x < -0.75f && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f && currentColumn > 0)
                {
                    LeftSwipe();
                }
                else
                {
                    selected = true;
                    selectionObj.SetActive(true);
                    CheckSelected();
                }

                //swipe right
                if (currentSwipe.x > 0.75f && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f && currentColumn < maxColumn)
                {
                    RightSwipe();
                }
                else
                {
                    selected = true;
                    selectionObj.SetActive(true);
                    CheckSelected();
                }

                checkSecondPos = false;
            }

            Ray tempCamRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit tempHitting;
            tempCheckAbove = true;
            tempCheckBelow = true;
            tempCheckLeft = true;
            tempCheckRight = true;

            if (Physics.Raycast(tempCamRay, out tempHitting, camRayLenght, jewelMask))
            {
                if (tempHitting.transform.position.y > transform.position.y + 1)
                {
                    tempCheckAbove = false;
                }
                if (tempHitting.transform.position.y < transform.position.y - 1)
                {
                    tempCheckBelow = false;
                }

                if (tempHitting.transform.position.x > transform.position.x + 1)
                {
                    tempCheckRight = false;
                }
                if (tempHitting.transform.position.x < transform.position.x - 1)
                {
                    tempCheckLeft = false;
                }

                if (!tempCheckAbove || !tempCheckBelow || !tempCheckLeft || !tempCheckRight)
                {
                    selected = false;
                    selectionObj.SetActive(false);
                }
            }
        }
    }

    void MovementRaycast()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(camRay, out buttonHit, camRayLenght, jewelMask))
        {
            if (buttonHit.transform == gameObject.transform)
            {
                iWasTheOneWhoCalled = true;
                whoCalledMe = null;
                repositionJewel = false;
                repositionJewelConfirmation = false;

                checkSecondPos = true;
                GetFirstMousePos();
            }
        }
    }

    void GetFirstMousePos()
    {
        mouseWorldPosition = cam.ScreenToWorldPoint(Input.mousePosition);

        firstPressPos = new Vector2(mouseWorldPosition.x, mouseWorldPosition.y);
    }

    public void CheckJewelsAround()
    {
        if (!iAlreadyChecked)
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out upHit, checkRayLenght, jewelMask))
            {
                if (upHit.transform.tag == gameObject.tag)
                {
                    objAbove = upHit.transform.gameObject;

                    if (objAbove.GetComponent<JewelManager>().whoCalledMe == null)
                    {
                        objAbove.GetComponent<JewelManager>().whoCalledMe = transform.GetComponent<JewelManager>();
                        objAbove.GetComponent<JewelManager>().callOtherRaycast = true;
                    }

                }
            }

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out downHit, checkRayLenght, jewelMask))
            {
                if (downHit.transform.tag == gameObject.tag)
                {
                    objBelow = downHit.transform.gameObject;

                    if (objBelow.GetComponent<JewelManager>().whoCalledMe == null)
                    {
                        objBelow.GetComponent<JewelManager>().whoCalledMe = transform.GetComponent<JewelManager>();
                        objBelow.GetComponent<JewelManager>().callOtherRaycast = true;
                    }

                }
            }

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out rightHit, checkRayLenght, jewelMask))
            {
                if (rightHit.transform.tag == gameObject.tag)
                {
                    objRight = rightHit.transform.gameObject;

                    if (objRight.GetComponent<JewelManager>().whoCalledMe == null)
                    {
                        objRight.GetComponent<JewelManager>().whoCalledMe = transform.GetComponent<JewelManager>();
                        objRight.GetComponent<JewelManager>().callOtherRaycast = true;
                    }

                }
            }

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out leftHit, checkRayLenght, jewelMask))
            {
                if (leftHit.transform.tag == gameObject.tag)
                {
                    objLeft = leftHit.transform.gameObject;

                    if (objLeft.GetComponent<JewelManager>().whoCalledMe == null)
                    {
                        objLeft.GetComponent<JewelManager>().whoCalledMe = transform.GetComponent<JewelManager>();
                        objLeft.GetComponent<JewelManager>().callOtherRaycast = true;
                    }

                }
            }

            iAlreadyChecked = true;
        }

        TellTheOthersIFoundMatch();

        StartCoroutine(WaitToCheckMatch());

        callOtherRaycast = false;
    }

    public void InformationPassed()
    {
        otherJewelManager.futurePos.x = otherJewelManager.currentColumn;
        otherJewelManager.futurePos.y = otherJewelManager.currentLine;
        otherJewelManager.previousPos.x = Mathf.Round(otherJewelManager.transform.position.x);
        otherJewelManager.previousPos.y = Mathf.Round(otherJewelManager.transform.position.y);
        otherJewelManager.whoCalledMe = GetComponent<JewelManager>();
        otherJewelManager.iWasTheOneWhoCalled = false;
        otherJewelManager.repositionJewel = false;
        otherJewelManager.repositionJewelConfirmation = false;
        otherJewelManager.moveJewel = true;
    }

    IEnumerator WaitToCheckMatch()
    {
        yield return new WaitForSeconds(.5f);

        if (!foundMatchVer && !foundMatchHor)
        {
            if (objAbove != null)
            {
                if (objAbove.GetComponent<JewelManager>().foundMatchVer)
                {
                    if ((int)transform.position.x == (int)objAbove.transform.position.x)
                    {
                        foundMatchVer = true;
                    }
                }
            }

            if (objBelow != null)
            {
                if (objBelow.GetComponent<JewelManager>().foundMatchVer)
                {
                    if ((int)transform.position.x == (int)objBelow.transform.position.x)
                    {
                        foundMatchVer = true;
                    }
                }
            }

            if (objLeft != null)
            {
                if (objLeft.GetComponent<JewelManager>().foundMatchHor)
                {
                    if ((int)transform.position.y == (int)objLeft.transform.position.y)
                    {
                        foundMatchHor = true;
                    }
                }
            }

            if (objRight != null)
            {
                if (objRight.GetComponent<JewelManager>().foundMatchHor)
                {
                    if ((int)transform.position.y == (int)objRight.transform.position.y)
                    {
                        foundMatchHor = true;
                    }
                }
            }
        }

        if (foundMatchVer || foundMatchHor)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetComponent<BoxCollider>().enabled = false;
            //Destroy(gameObject);

            RaycastHit tempUpHit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out tempUpHit, 1f, jewelMask))
            {
                tempUpHit.transform.GetComponent<JewelManager>().MakeObjectsGoDown();
            }

            GetAllObjectsAbove();

            iAlreadyChecked = false;
            callOtherRaycast = true;

            Instantiate(explosion, transform.position, Quaternion.identity);

            GetAllObjectsAbove();
            justTellOnce = false;
            AskToCreateANewJewel();

            TellTheOthersIFoundMatch();

            Destroy(gameObject);
        }

        if (otherJewelManager != null)
        {
            if (!otherJewelManager.foundMatchVer && !otherJewelManager.foundMatchHor)
            {
                if (!foundMatchHor && !foundMatchVer)
                {
                    if (iWasTheOneWhoCalled)
                    {
                        otherJewelManager.repositionJewel = true;
                        otherJewelManager.repositionJewelConfirmation = true;
                        repositionJewelConfirmation = true;
                        repositionJewel = true;
                        iWasTheOneWhoCalled = false;

                        objLeft = null;
                        objAbove = null;
                        objBelow = null;
                        objRight = null;
                    }
                }
            }
        }
    }

    public void TellTheOthersIFoundMatch()
    {
        if (objAbove != null && objBelow != null)
        {
            if (objAbove.tag == gameObject.tag && objBelow.tag == gameObject.tag)
            {
                foundMatchVer = true;
                objAbove.GetComponent<JewelManager>().foundMatchVer = true;
                objBelow.GetComponent<JewelManager>().foundMatchVer = true;
            }
        }

        if (objRight != null && objLeft != null)
        {
            if (objRight.tag == gameObject.tag && objLeft.tag == gameObject.tag)
            {
                foundMatchHor = true;
                objRight.GetComponent<JewelManager>().foundMatchHor = true;
                objLeft.GetComponent<JewelManager>().foundMatchHor = true;
            }
        }
    }

    public void GetAllObjectsAbove()
    {
        if (transform.position.y < boardManager.height -1)
        {
            RaycastHit tempUpHit;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out tempUpHit, 10f, jewelMask))
            {
                tempUpHit.transform.GetComponent<JewelManager>().MakeObjectsGoDown();
            }
            else
            {
                
            }
        }
    }

    public void MakeObjectsGoDown()
    {
        tryAgain = false;

        if (!foundMatchVer)
        {
            RaycastHit tempDownHit;

            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out tempDownHit, 10f, jewelMask))
            {
                moveJewelDown = false;
                moveDownPos.y = Mathf.Abs(tempDownHit.transform.position.y) + 1;
                moveDownPos.x = Mathf.Abs(tempDownHit.transform.position.x);

                moveJewelDown = true;
            }
            else
            {
                //tryAgain = true;
                /*
                moveDownPos.y = 0;
                moveDownPos.x = Mathf.Abs(transform.position.x);

                moveJewelDown = true;
                */
            }
        }
    }

    public void AskToCreateANewJewel()
    {
        boardManager.newStartingPos.x = Mathf.RoundToInt(transform.position.x);
        boardManager.newStartingPos.y = Mathf.RoundToInt(transform.position.y + 1);

        boardManager.newJewelPos.x = Mathf.RoundToInt(transform.position.x);
        boardManager.CreateNewJewel();
    }

    public void UpSwipe()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out jewelHitted, moveRayLenght, jewelMask))
        {
            selected = false;
            selectionObj.SetActive(false);
            currentLine++;
            futurePos.x = currentColumn; // Mathf.Round(currentLine);
            futurePos.y = currentLine; // Mathf.Round(currentLine);
            moveJewel = true;
            otherJewelManager = jewelHitted.transform.GetComponent<JewelManager>();
            otherJewelManager.currentLine--;

            InformationPassed();
        }
    }

    public void DownSwipe()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out jewelHitted, moveRayLenght, jewelMask))
        {
            selected = false;
            selectionObj.SetActive(false);
            currentLine--;
            futurePos.x = currentColumn; // Mathf.Round(currentLine);
            futurePos.y = currentLine; // Mathf.Round(currentLine);
            moveJewel = true;
            otherJewelManager = jewelHitted.transform.GetComponent<JewelManager>();
            otherJewelManager.currentLine++;

            InformationPassed();
        }
    }

    public void LeftSwipe()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out jewelHitted, moveRayLenght, jewelMask))
        {
            selected = false;
            selectionObj.SetActive(false);
            currentColumn--;
            futurePos.x = currentColumn; // Mathf.Round(currentLine);
            futurePos.y = currentLine; // Mathf.Round(currentLine);
            moveJewel = true;
            otherJewelManager = jewelHitted.transform.GetComponent<JewelManager>();
            otherJewelManager.currentColumn++;

            InformationPassed();
        }
    }

    public void RightSwipe()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out jewelHitted, moveRayLenght, jewelMask))
        {
            selected = false;
            selectionObj.SetActive(false);
            currentColumn++;
            futurePos.x = currentColumn; // Mathf.Round(currentLine);
            futurePos.y = currentLine; // Mathf.Round(currentLine);
            moveJewel = true;
            otherJewelManager = jewelHitted.transform.GetComponent<JewelManager>();
            otherJewelManager.currentColumn--;

            InformationPassed();
        }
    }

    public void CheckSelected()
    {
        if (selected)
        {
            {
                RaycastHit tempUp;
                RaycastHit tempDown;
                RaycastHit tempLeft;
                RaycastHit tempRight;

                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out tempUp, 1f, jewelMask))
                {
                    if (tempUp.transform.GetComponent<JewelManager>().selected == true)
                    {
                        UpSwipe();
                        tempUp.transform.GetComponent<JewelManager>().selected = false;
                        tempUp.transform.GetComponent<JewelManager>().selectionObj.SetActive(false);
                    }
                }

                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out tempDown, 1f, jewelMask))
                {
                    if (tempDown.transform.GetComponent<JewelManager>().selected == true)
                    {
                        DownSwipe();
                        tempDown.transform.GetComponent<JewelManager>().selected = false;
                        tempDown.transform.GetComponent<JewelManager>().selectionObj.SetActive(false); ;
                    }
                }

                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out tempLeft, 1f, jewelMask))
                {
                    if (tempLeft.transform.GetComponent<JewelManager>().selected == true)
                    {
                        LeftSwipe();
                        tempLeft.transform.GetComponent<JewelManager>().selected = false;
                        tempLeft.transform.GetComponent<JewelManager>().selectionObj.SetActive(false); ;
                    }
                }

                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out tempRight, 1f, jewelMask))
                {
                    if (tempRight.transform.GetComponent<JewelManager>().selected == true)
                    {
                        RightSwipe();
                        tempRight.transform.GetComponent<JewelManager>().selected = false;
                        tempRight.transform.GetComponent<JewelManager>().selectionObj.SetActive(false); ;
                    }
                }
            }
        }
    }
}