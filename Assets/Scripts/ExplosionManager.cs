﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{
    public Animator explosionAnim;

    void Start()
    {
        
    }

    void Update()
    {
        if (explosionAnim.GetCurrentAnimatorStateInfo(0).IsName("Explosion_End"))
        {
            Destroy(gameObject);
        }
    }
}