﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int mainMenuScene;
    public int gameScene;

    void Start()
    {

    }

    void Update()
    {

    }

    public void CloseGame()
    {
        SceneManager.LoadScene(mainMenuScene);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(gameScene);
    }
}