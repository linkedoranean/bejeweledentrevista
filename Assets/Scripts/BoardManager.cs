﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public int height;
    public int width;
    
    public GameObject[] jewelManager;

    public GameObject[,] allJewels;

    [Header("Initial Setup")]
    public float delayBetweenJewels;

    [Header("Creating Extra Jewels")]
    public Vector2 newJewelPos;
    public Vector2 newStartingPos;

    void Start()
    {
        //BoardSetup();
        allJewels = new GameObject[height,width];
        StartCoroutine(BoardSetup());
    }

    void Update()
    {
        
    }

    IEnumerator BoardSetup()
    {
        for (int j = 0; j < width; j++)
        {
            for (int i = 0; i < width; i++)
            {
                yield return new WaitForSeconds(delayBetweenJewels);
                Vector2 startingPos = new Vector2(i, j);
                Vector2 tempPosition = new Vector2(i, j + 14f);
                int prefabSelected = Random.Range(0, jewelManager.Length);

                int whileHolder = 0;
                while (CheckToAvoidMatchs(i, j, jewelManager[prefabSelected]))
                {
                    prefabSelected = Random.Range(0, jewelManager.Length);
                    whileHolder++;
                }

                whileHolder = 0;

                GameObject positionReference = Instantiate(jewelManager[prefabSelected], tempPosition, Quaternion.identity).gameObject;
                positionReference.GetComponent<JewelManager>().startingPos = startingPos;
                positionReference.transform.parent = gameObject.transform;
                allJewels[i, j] = positionReference;

            }
        }
    }

    private bool CheckToAvoidMatchs(int column, int line, GameObject jewelCreated)
    {
        if (column > 1 && line > 1)
        {
            if (allJewels[column - 1, line].tag == jewelCreated.tag && allJewels[column - 2, line].tag == jewelCreated.tag)
            {
                return true;
            }

            if (allJewels[column, line - 1].tag == jewelCreated.tag && allJewels[column, line - 2].tag == jewelCreated.tag)
            {
                return true;
            }
        }

        else if (column <= 1 || line <= 1)
        {
            if (line > 1)
            {
                if (allJewels[column, line -1].tag == jewelCreated.tag && allJewels[column, line - 2].tag == jewelCreated.tag)
                {
                    return true;
                }
            }

            if (column > 1)
            {
                if (allJewels[column -1, line].tag == jewelCreated.tag && allJewels[column -2, line].tag == jewelCreated.tag)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void CreateNewJewel()
    {
        Vector2 tempPosition = newJewelPos;
        tempPosition.y += 14f;
        int prefabSelected = Random.Range(0, jewelManager.Length);

        GameObject positionReference = Instantiate(jewelManager[prefabSelected], tempPosition, Quaternion.identity).gameObject;
        positionReference.GetComponent<JewelManager>().startingPos = newStartingPos;
        positionReference.transform.parent = gameObject.transform;
        positionReference.GetComponent<JewelManager>().setupJewel = false;

    }
}